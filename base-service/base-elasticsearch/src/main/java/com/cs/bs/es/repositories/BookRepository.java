/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: BookRepository
 * Author:   liyuan
 * Date:     2019-03-22 18:41
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.bs.es.repositories;

import com.cs.bs.es.entity.Book;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author liyuan
 * @create 2019-03-22
 * @since 1.0.0
 */
@Repository
public interface BookRepository extends ElasticsearchRepository<Book, String> {


    List<Book> findBooksByName(String name);

    List<Book> findBooksByAuthor(String author);

    Book findBookById(String id);

    List<Book> findAllByName(String name);

    List<Book> findAllByAuthor(String author);

    Iterable<Book> search(QueryBuilder builder);
}