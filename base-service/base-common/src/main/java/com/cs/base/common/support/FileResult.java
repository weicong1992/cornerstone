package com.cs.base.common.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wangjiahao
 * @version 1.0
 * @className FileResult
 * @since 2019-04-03 10:49
 */
@Data
@ApiModel("文件返回对象")
public class FileResult implements Serializable {

    @ApiModelProperty("原文件名")
    private String filename;
    @ApiModelProperty("返回的标题")
    private String title;
    @ApiModelProperty("文件访问路径")
    private String url;

}
