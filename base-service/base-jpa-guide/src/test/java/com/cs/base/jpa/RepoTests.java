package com.cs.base.jpa;

import com.cs.base.jpa.entity.Power;
import com.cs.base.jpa.entity.Role;
import com.cs.base.jpa.entity.User;
import com.cs.base.jpa.repository.PowerRepo;
import com.cs.base.jpa.repository.RoleRepo;
import com.cs.base.jpa.repository.UserRepo;
import com.cs.base.jpa.support.Gender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * @author wangjiahao
 * @version 1.0
 * @className RepoTests
 * @since 2019-03-29 16:19
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JpaGuideApplication.class)
public class RepoTests {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private PowerRepo powerRepo;

    /**
     * 初始化插入数据
     */
    @Test
    public void init() {
        User user = new User();
        user.setName("test");
        user.setGender(Gender.Male);

        Role role = new Role();
        role.setName("testRole");

        Power power1 = new Power();
        power1.setName("p1");
        power1.setCode("code1");

        Power power2 = new Power();
        power2.setName("p2");
        power2.setCode("code2");

        Power power3 = new Power();
        power3.setName("p3");
        power3.setCode("code3");

        HashSet<Power> powers = new HashSet<>();
        powers.add(power1);
        powers.add(power2);
        powers.add(power3);

        role.setPowers(powers);

        user.setRole(role);

        User save = userRepo.save(user);
        System.out.println("save = " + save);
    }

    /**
     * 注意这里使用的调用方式
     * 1.这里其实只用了一个jpa的查询方法，但是实际执行了4个SQL语句，可以在日志里看到，
     * 所以，一定要保持这一整个过程中在同一事务中，否则就会报出关于懒加载的错误
     * 2.注意实体类的嵌套关系，jpa的关系中存在，A包含B，B中又存在A的情况，如果调用没有
     * 特别注意的话，就会出现无限循环，从而导致内存溢出等问题。
     * <p>
     * 解决方式，
     * 1.确定主次关系，在次类中注解@JsonIgnore
     * 2.调用的时候，只调用需要的部分，如下代码，只调用2部分，关于role和power的查询是不会执行的
     * 3.在使用懒加载时，如果执行代码没有执行到相关属性，是不会查询对应数据库的，例如下面执行到3时，
     * 才会联查power；而执行1时，因为user和role是一对一，也没有使用懒加载，所以会一起查询出来
     */
    @Test
    @Transactional
    public void userTest() {
//        1
        List<User> list = userRepo.findAll();
        System.out.println("list.size() = " + list.size());
        System.out.println("user = " + list.get(0).getName());
//        2
        Role role = list.get(0).getRole();
        System.out.println("role = " + role.getName());
//        3
        Set<Power> powers = role.getPowers();
        System.out.println("powers.size = " + powers.size());
    }

    /**
     * 下面是从role作为开始的，步骤和上面一致
     */
    @Test
    @Transactional
    public void roleTest() {
//        1
        List<Role> roles = roleRepo.findAll();
        Role role = roles.get(0);
        System.out.println("role.getName() = " + role.getName());
//        2
        User user = role.getUser();
        System.out.println("user.getName() = " + user.getName());
//        3
        Set<Power> powers = role.getPowers();
        System.out.println("powers.size() = " + powers.size());
    }

    /**
     * 下面是从power开始查询，可以从控制台看到，从2开始，直接一句SQL就查询出了user和role
     */
    @Test
    @Transactional
    public void powerTest() {
//        1
        List<Power> powers = powerRepo.findAll();
        System.out.println("powers.size() = " + powers.size());
//        2
        Set<Role> roles = powers.get(0).getRoles();
        Role role = roles.iterator().next();
        System.out.println("role.getName() = " + role.getName());
//        3
        User user = role.getUser();
        System.out.println("user = " + user.getName());
    }

    /**
     * 分页查询，页码从0开始
     * 1.最普通的分页查询
     * 2.带有排序的分页查询
     * 3.按条件的分页查询
     * <p>
     * 在此方法中，因为有查询缓存的原因，所以对三个方法的count查询，只执行了一次
     */
    @Test
    public void powerPageTest() {
//        1
        PageRequest page1 = PageRequest.of(0, 2);
        Page<Power> pageResult1 = powerRepo.findAll(page1);
        System.out.println("总记录数 = " + pageResult1.getTotalElements());
        System.out.println("总页码 = " + pageResult1.getTotalPages());
        System.out.println("当前页码 = " + pageResult1.getNumber());
        System.out.println("页面大小 = " + pageResult1.getSize());
        System.out.println("内容 : ");
        pageResult1.getContent().forEach(p -> System.out.println(p.getName() + "--" + p.getCode()));

        System.out.println("=================================== ");

//        2
        PageRequest page2 = PageRequest.of(0, 5, Sort.by(Sort.Order.desc("code")));
        Page<Power> pageResult2 = powerRepo.findAll(page2);
        System.out.println("总记录数 = " + pageResult2.getTotalElements());
        System.out.println("总页码 = " + pageResult2.getTotalPages());
        System.out.println("当前页码 = " + pageResult2.getNumber());
        System.out.println("页面大小 = " + pageResult2.getSize());
        System.out.println("内容: ");
        pageResult2.getContent().forEach(p -> System.out.println(p.getName() + "--" + p.getCode()));

        System.out.println("=================================== ");

//        3
        Power power = new Power();
        power.setCode("code1");
        Example<Power> example = Example.of(power, ExampleMatcher.matching());
        Page<Power> pageResult3 = powerRepo.findAll(example, page2);
        System.out.println("总记录数 = " + pageResult3.getTotalElements());
        System.out.println("总页码 = " + pageResult3.getTotalPages());
        System.out.println("当前页码 = " + pageResult3.getNumber());
        System.out.println("页面大小 = " + pageResult3.getSize());
        System.out.println("内容: ");
        pageResult3.getContent().forEach(p -> System.out.println(p.getName() + "--" + p.getCode()));
    }


    /**
     * 使用jpa之后，会自动注入{@link EntityManager}EntityManager
     */
    @Autowired
    private EntityManager entityManager;

    /**
     * 普通分组查询,select name ,count(id) from power where name like 'p1' group by name
     */
    @Test
    @Transactional
    public void customQueryTest() {
//        创建查询builder
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        指定查询结果对象，也就是最后会映射的对象
        CriteriaQuery<Result> query = cb.createQuery(Result.class);
//        指定查询对象，也就是查询的表
        Root<Power> root = query.from(Power.class);
//        指定查询的字段，如果是普通字段，指定root.get("属性"),如果是函数，就用cb创建
        query.multiselect(root.get("name"), cb.count(root.get("id")));
//        指定where条件
        Predicate whereLike = cb.like(root.get("name"), "p1");
        query.where(whereLike);
//        指定分组
        query.groupBy(root.get("name"));
//        获取结果
        List<Result> list = entityManager.createQuery(query).getResultList();

        list.forEach(r -> System.out.println(r.getName() + ":" + r.getCount()));

    }

    /**
     * 普通关联分组查询：
     * select p.name ,count(p.id)
     * from t_power p
     * left join t_role_powers trp on p.id = trp.powers_id
     * left join t_role tr on tr.id = trp.role_id
     * where tr.name = 'testRole' group by p.name
     */
    @Test
    @Transactional
    public void customJoinQueryTest() {
//        创建查询builder
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        指定查询结果对象，也就是最后会映射的对象
        CriteriaQuery<Result> query = cb.createQuery(Result.class);
//        指定查询对象，也就是查询的表
        Root<Power> root = query.from(Power.class);
//        指定查询的字段，如果是普通字段，指定root.get("属性"),如果是函数，就用cb创建
        query.multiselect(root.get("name"), cb.count(root.get("id")));
//        指定关联属性，查询的主类中需要包含子类，如power中的roles
        Join<Power, Role> roles = root.join("roles", JoinType.LEFT);
        Predicate equal = cb.equal(roles.get("name"), "testRole");
        query.where(equal);
//        指定分组
        query.groupBy(root.get("name"));
//        获取结果
        List<Result> list = entityManager.createQuery(query).getResultList();

        list.forEach(r -> System.out.println(r.getName() + ":" + r.getCount()));
    }

    /**
     * 关联查询--使用repository接口实现
     * <p>
     * SQl语句：
     * SELECT
     * u.id AS id,
     * u.gender AS gender,
     * u.NAME AS name,
     * u.role_id AS role_id
     * FROM t_user u
     * LEFT OUTER JOIN t_role r ON u.role_id = r.id
     * WHERE
     * u.id = 5
     * AND r.NAME =?
     * <p>
     * 1.必须在repository接口上继承{@link JpaSpecificationExecutor}JpaSpecificationExecutor<?>
     * 2.继承此接口之后，可以使用关于{@link Specification} specification的查询方法，这个方法是一种动态的构造查询参数的方法
     * 3.使用关联查询时，主类中需要包含子类，如user中包含role，他俩是一对一的关系
     */
    @Test
    @Transactional
    public void joinQueryTest() {

//        Specification接口没有实现或构造方法，只能这么实现,可以自行封装实现工具类
        Specification<User> specification = new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                Join<User, Role> join = root.join("role", JoinType.LEFT);
                list.add(criteriaBuilder.equal(root.get("id"), "6"));
                list.add(criteriaBuilder.equal(join.get("name"), "testRole"));
                Predicate[] p = new Predicate[list.size()];
                query.where(criteriaBuilder.and(list.toArray(p)));
                return query.getRestriction();
            }
        };

        Optional<User> one = userRepo.findOne(specification);

        System.out.println("one.get().getName() = " + one.get().getName());
    }


    /**
     * 内部类，接收自定义结果
     */
    static class Result {
        private String name;
        private Long count;

        public Result(String name, Long count) {
            this.name = name;
            this.count = count;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getCount() {
            return count;
        }

        public void setCount(Long count) {
            this.count = count;
        }
    }
}
