package com.cs.base.jpa.repository;

import com.cs.base.jpa.entity.Power;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author wangjiahao
 * @version 1.0
 * @className PowerRepo
 * @since 2019-03-29 16:44
 */
@Repository
public interface PowerRepo extends JpaRepository<Power,Long> {
}
