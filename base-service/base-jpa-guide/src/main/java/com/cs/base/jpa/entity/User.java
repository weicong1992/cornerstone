package com.cs.base.jpa.entity;

import com.cs.base.jpa.support.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 * @author wangjiahao
 * @version 1.0
 * @className User
 * @since 2019-03-29 14:48
 * 这里要注意，如果使用lombok的情况，会默认编译出toString()，这个toString方法会包含嵌套的子类，
 * 容易产生无限嵌套的情况，所以这里不建议使用@Data这个注解
 */
@Setter
@Getter
@Entity(name = "t_user")
public class User {

    /**
     * mysql主键自增策略
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    /**
     * 可以使用枚举类型作为属性，jpa会自动把值映射到枚举类对应的值上
     */
    private Gender gender = Gender.Male;


    /**
     * 最普通的一对一设置方式，没有使用懒加载
     */
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private Role role;

    /**
     * 不会被持久化的字段
     */
    @Transient
    private String status = "0";

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", role=" + role +
                ", status='" + status + '\'' +
                '}';
    }
}
