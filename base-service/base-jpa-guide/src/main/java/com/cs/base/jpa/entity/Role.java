package com.cs.base.jpa.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.Set;

/**
 * @author wangjiahao
 * @version 1.0
 * @className Role
 * @since 2019-03-29 16:25
 */
@Getter
@Setter
@Entity(name = "t_role")
public class Role {

    /**
     * 使用UUID作为ID，
     * 使用{@link org.hibernate.id.UUIDHexGenerator}UUIDHexGenerator生成策略
     */
    @Id
    @GenericGenerator(strategy = "uuid",name = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;
    private String name;

    @OneToOne(mappedBy = "role")
    private User user;

    /**
     * 最简单的多对多设置方式，并使用懒加载
     */
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<Power> powers;
}
