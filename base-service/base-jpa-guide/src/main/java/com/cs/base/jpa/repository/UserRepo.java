package com.cs.base.jpa.repository;

import com.cs.base.jpa.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author wangjiahao
 * @version 1.0
 * @className UserRepo
 * @since 2019-03-29 16:19
 */
@Repository
public interface UserRepo extends JpaRepository<User,Long>, JpaSpecificationExecutor<User> {
}
