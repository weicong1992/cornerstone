package com.cs.base.jpa.support;

/**
 * @author wangjiahao
 * @version 1.0
 * @className Gender
 * @since 2019-03-29 16:17
 */
public enum Gender {
    /**
     * 男
     */
    Male,
    /**
     * 女
     */
    Female;
}
