package com.cs.base.quartz.support;

import lombok.Data;

/**
 * @author wangjiahao
 * @version 1.0
 * @className TaskInfoEntity
 * @since 2019-03-25 14:34
 */
@Data
public class TaskInfoEntity {
    /**
     * 增加或修改标识
     */
    private int id;

    /**
     * 任务名称,也是对应任务类的全路径名称
     */
    private String jobClassName;

    /**
     * 任务分组
     */
    private String jobGroup;

    /**
     * 任务描述
     */
    private String jobDescription;

    /**
     * 任务状态
     */
    private String jobStatus;

    /**
     * 任务表达式
     */
    private String cronExpression;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 间隔时间（毫秒）
     */
    private String milliSeconds;

    /**
     * 重复次数
     */
    private String repeatCount;

    /**
     * 起始时间
     */
    private String startDate;

    /**
     * 终止时间
     */
    private String endDate;
}
