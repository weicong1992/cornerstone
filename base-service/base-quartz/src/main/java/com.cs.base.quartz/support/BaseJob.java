package com.cs.base.quartz.support;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;

/**
 * 不允许并发的job
 * @author wangjiahao
 * @version 1.0
 * @className BaseJob
 * @since 2019-03-25 16:09
 */
@DisallowConcurrentExecution
public abstract class BaseJob implements Job {

    public abstract Option option();

}
