package com.cs.micro.demo.store.fallback;

import com.cs.base.common.support.FileResult;
import com.cs.base.common.support.SimpleResult;
import com.cs.base.common.support.SimpleStatusCode;
import com.cs.micro.demo.store.api.FileServerApi;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wangjiahao
 * @version 1.0
 * @className FileServerApiFallback
 * @since 2019-04-03 13:27
 */
@Component
public class FileServerApiFallback implements FileServerApi {
    @Override
    public SimpleResult<FileResult> uploadImage(MultipartFile file, String title) {
        return SimpleResult.build(SimpleStatusCode.FAIL);
    }
}
