package com.cs.micro.demo.quartz.config;

import com.cs.base.common.SpringBootContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 其他组件装配
 *
 * @author wangjiahao
 * @version 1.0
 * @className ComponentConfig
 * @since 2019-03-11 11:04
 */
@Configuration
public class ComponentConfig {

    @Bean
    public SpringBootContext springBootContext() {
        return new SpringBootContext();
    }

}
