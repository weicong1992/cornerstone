package com.cs.micro.demo.quartz.web;

import com.cs.base.quartz.core.QuartzManager;
import com.cs.base.quartz.support.TaskInfoEntity;
import com.cs.micro.demo.quartz.task.DemoTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjiahao
 * @version 1.0
 * @className QuartzCtrl
 * @since 2019-03-19 16:33
 */
@RestController
@RequestMapping("/quartz")
public class QuartzCtrl {

    @Autowired
    private QuartzManager manager;
    @Autowired
    private DemoTask demoTask;

    //
    @GetMapping("/start")
    public String pause() {
        TaskInfoEntity entity = new TaskInfoEntity();
        entity.setJobClassName(demoTask.option().getJobClassName());
        entity.setCronExpression(demoTask.option().getCron());
        entity.setJobGroup(demoTask.option().getGroupName());
        manager.addJob(entity);

        return "ok";
    }

    @GetMapping("stop")
    public String stop() {
        manager.pauseJob("com.cs.micro.demo.quartz.task.DemoTask","group1");
        return "ok";
    }

    @GetMapping("resume")
    public String resume() {
        manager.resumeJob("com.cs.micro.demo.quartz.task.DemoTask","group1");
        return "ok";
    }
}
