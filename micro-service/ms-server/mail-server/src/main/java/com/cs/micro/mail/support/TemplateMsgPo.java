package com.cs.micro.mail.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangjiahao
 * @version 1.0
 * @className SimpleMsgPo
 * @since 2019-03-28 11:20
 */
@Data
@ApiModel("模板邮件发送参数")
public class TemplateMsgPo {

    @ApiModelProperty("发送地址")
    private String email;

    @ApiModelProperty("发送主题")
    private String subject;

    @ApiModelProperty("内容")
    private String content;

}
