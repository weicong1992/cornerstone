package com.cs.micro.file.config;

import com.cs.base.common.SpringBootContext;
import com.github.tobato.fastdfs.FdfsClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;
import org.springframework.jmx.support.RegistrationPolicy;

/**
 * 其他组件装配
 *
 * @author wangjiahao
 * @version 1.0
 * @className ComponentConfig
 * @since 2019-03-11 11:04
 */
@Configuration
@Import(FdfsClientConfig.class)
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
@ComponentScan(basePackages = "com.cs.base.cloud.feign")
public class ComponentConfig {

    @Bean
    public SpringBootContext springBootContext() {
        return new SpringBootContext();
    }

}
