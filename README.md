# cornerstone
完整的微服务架构解决方案

### config-repo:配置文件仓库
用于微服务的配置中心

### base-service:单独的项目

|项目名称|作用|版本|
|:--|:--|:--|
| base-common | 基础工具包 | 1.0-SNAPSHOT |
| cms-project | CMS独立项目 | 1.0-SNAPSHOT |
| base-oauth2-rc | oauth2的client和resource-server统一配置组件 |1.0-SNAPSHOT|
| base-security | 独立security配置组件 |1.0-SNAPSHOT|
| base-cloud | 微服务基本依赖集合 |1.0-SNAPSHOT|
| base-quartz | quartz相关工具类 |1.0-SNAPSHOT|
| base-elasticsearch | es组件 |1.0-SNAPSHOT|
| base-jpa-guide | jpa简易demo |1.0-SNAPSHOT|

说明：`base-cloud`是一个依赖集合，包含一些简单的公共配置，以及以下依赖，有一些spring boot和cloud的可以无配置直接使用，有一些其他依赖需要注解、配置或javabean配置
除了会被自动装配的东西以外，如果需要使用到这里的配置类，需要根据这里配置类的使用方式导入。
```
本地工具包：
   base-common

微服务服务依赖包 begin：
   spring-cloud-starter-netflix-hystrix
   spring-cloud-starter-netflix-eureka-client
   spring-boot-starter-web
   spring-boot-starter-test

spring config client动态刷新配置依赖：
   spring-cloud-config-client
   spring-cloud-bus
   spring-cloud-stream-binder-rabbi
   spring-boot-starter-actuator

lombok：
   lombok

swagger：
   swagger-bootstrap-ui
   springfox-swagger2

分布式锁：
   redisson
微服务服务依赖包 end
       
分布式事务：
   txlcn-tc
   txlcn-txmsg-netty
```

### micro-service:微服务基础服务群

*项目环境:spring boot 2.0.3.RELEASE, spring cloud Finchley.SR1,
 jdk8+, mysql5.5+, rabbitmq, redis，lcn* 


|服务类型|项目名称|作用|版本|
|:--|:--|:--|:--|
|基础服务|eureka-server|eureka注册中心|1.0-SNAPSHOT|
|基础服务|remote-config-server|统一配置中心|1.0-SNAPSHOT|
|基础服务|gateway-server|网关|1.0-SNAPSHOT|
|基础服务|oauth2-server|授权认证中心|1.0-SNAPSHOT|
|基础服务|tx-manager|LCN分布式事务管理服务器|1.0-SNAPSHOT|
|基础服务|admin-server|spring boot admin server|1.0-SNAPSHOT|
|基础服务|mail-server|邮件发送服务|1.0-SNAPSHOT|
|基础服务|file-server|文件管理服务|1.0-SNAPSHOT|
|------|------|------|------|
|业务服务|demo-store|商店demo|1.0-SNAPSHOT|
|业务服务|demo-customer|顾客demo|1.0-SNAPSHOT|
|业务服务|demo-quartz1|定时任务服务1|1.0-SNAPSHOT|
|业务服务|demo-quartz2|定时任务服务2|1.0-SNAPSHOT|

启动顺序以及其他注意：
1. 启动前请检查redis，mysql，rabbitmq是否启动，建议使用docker启动
2. eureka-server-->remote-config-server必须的
3. 后面可以根据依赖关系启动
4. 如果需要使用分布式事务，请务必在第一步以后，启动tx-manager

*注意*：
1. 除了`配置中心`和`eureka-server`以外，其他服务均使用配置中心的配置文件，所以他们两个一定要提前启动。
2. 在仓库中修改了配置文件以后，注意一定要提交，然后使用配置中心的`/actuator/bus-refresh`去做刷新配置的操作。
3. 配置刷新以后，如果只是普通参数，可以在`@RefreshScope`的作用刷新，如果是类似端口号一类的配置，需要重启对应服务

## 分布式事务
分布式事务使用LCN技术，需要使用的话，引入`txlcn-tc`和`txlcn-txmsg-netty`依赖，并在启动类上注解`@EnableDistributedTransaction`,这样lcn已经可以使用了。
其次，在对应事务开始的方法上加上`@LcnTransaction`才能保证此次操作使用了lcn的事务。


## 分布式调度
分布式调度使用了quartz和springcloud集成在一起。启动时，请引入`base-quartz`依赖，并注解`@EnableQuartz`在启动类上。

具体调度的任务类需要继承`BaseJob`此类，并实现他的`option()`方法，这个方法里包含了他的`group`,`jobName`,`jobClassName`和`cron`属性。

定时任务可以使用`QuartzManager`实现动态修改。

*需要注意的是，现在的配置中，没太深究其原因，导致启动的时候，定时任务并不会自动被添加到数据库中，所以使用了一个监听器`QuartzListener`来保证启动的时候对所有符合要求的任务进行添加。*

另外，集群环境下的定时任务和单个项目的定时任务使用不冲突，单机定时任务就按照springboot的使用方式就可以。具体代码示例参看`demo-quartz1`和`demo-quartz2`

## Zipkin和Sleuth链路追踪
使用zipkin-server作为追踪服务，现在官方不推荐使用自定义的zipkinserver，所以我们使用了docker启动zipkin-server，用官方jar包启动也是一样的。
本项目中没有使用任何持久化的记录方式，所以不需要添加数据库，jdbc或是mq等配置，如需使用，自行配置或后续改进中会增加。

docker启动命令:`docker run -d -p 9411:9411 openzipkin/zipkin`，启动后访问`ip:9411`即可

另外，在本项目中，示例都放在了`demo-store`和`demo-customer`中，因此，需要启动的项目有如下：
1. `eureka-server`
2. `remote-config-server`
3. `tx-manager`
4. `oauth2-server`
5. `demo-store`和`demo-customer`
 
---

## Spring-Boot-Admin-Server
在项目中使用spring boot admin server进行监控，并使用security对admin-server做了简单登录认证。对于被监控的项目来说，需要用`spring-boot-starter-actuator`依赖，对外开放一些可监控内容，例如info,
health等，根据情况自行设置，但因为使用了config配置中心，以及消息总线，请一定对外开放`bus-refresh`。如果没有打开那些内容，那么在admin-server的UI上访问到相应内容时，控制台可能会报错，但没有不良影响。

## MAIL-SERVER
邮件发送服务配置了QQ的邮件发送服务器，可以按需要修改。现在将它配置为基础服务，需要使用feign去做服务间的调用。现在做了两个发送邮件的demo，一是普通文本，一是使用freemarker模板。
在本项目中使用发送邮件功能，需要提前启动`eureka-server`,`config-server`和`oauth2-server`三个服务。

## FILE-SERVER
文件管理服务，使用fastfds和nginx作为文件服务器，然后可以从这个服务里把文件上传到服务器中。其他服务需要上传文件时，都从此项目中上传，如果是代码中上传，用feign引用这个服务中的接口即可。

*注意：需要自己配置tracker和nginx的地址。在使用feignclient的时候，需要用到他的扩展jar包，`feign-form`和`feign-form-spring`,需要注意版本兼容。*

## 消息队列
项目中使用springboot2.x+rabbbitmq实现消息队列功能，支持Direct、Topic、Fanout三种模式，配置类为RabbitConfig，可按需配置，生产者demo可以再demo-store中看到；消费者demo在demo-customer中。

## Elasticsearch
Java整合es三种模式中，官方已经明确表示在ES 7.0版本中将弃用TransportClient客户端，项目中不用此方法实现；第二种基于springData的实现一套方案spring-data-elastixsearch，版本之间的搭配建议为：

|spring data elasticsearch|elasticsearch|
|:--|:--| 
|3.1.x|6.2.2|
|3.0.x|5.5.0|
|2.1.x|2.4.0|
|2.0.x|2.2.0|
|1.3.x|1.5.2|   
  
这种集成方式简单，适用于简单业务，项目中就用了此方法；第三中方式使用Rest client，为官方推荐，后续会添加此实现。

## 后续计划
micro-service：
- [x] 增加redis分布式锁
- [x] 增加分布式事务
- [x] 增加定时任务
- [x] 增加消息队列
- [x] 增加链路追踪
- [x] 增加服务监控
- [x] 文件管理服务
- [x] 邮件服务
- [ ] 与docker部署结合
- [ ] ELK日志系统
- [ ] 文档

base-service：
- [ ] 完善base-common
- [x] 增加base-ElasticSearch配置